/*
Provides methods of finding duplicate files.


*/

package dup

import (
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type fileHash struct {
	path string
	hash []byte
}

type MatchType int

//Group type constants
const (
	Filename Matchtype = 1 << iota
	Hash
	Identical
	DiffWhitespace
	DiffNearMatch
	ImageDiffNear
)

//Constants used internally
const (
	filename int = iota
	hash
)

//Interface for groups of identical files
type Group struct {
	match MatchType
	paths []string
	certainty float32
}

//Internal datastore
type eqnInfo []map[string]interface{}

func (info eqnInfo) todoThisFunctionNeedsAName() {
	
}

// Finds groups of duplicated files. The groups have a type that
// specifies why the files are grouped. Identical filenames?
// Identical hash? Identical contents?
func Find(dirs []string) []Group {
	paths := make(chan string)
	
	walkFiles(dirs, paths)
	done := processFiles(paths)
	groups = computeGroups(<-done)
	
	return groups
}

func processFiles(paths <-chan string) <- chan eqnInfo {
	info := make(chan eqnInfo)
	
	//Process files...
	go process(paths, info)
	
	return info;
}

func process(paths <-chan string, info chan <- eqnInfo) {
	var info eqnInfo = make([]map[string]interface{}, 2)
	info[filename] = make(map[string]interface{})
	info[hash] = make(map[string]interface{})
	
}

func walkFiles(dirs []string, paths chan<- string) {
	go func() {
		walker := getWalker(paths);
	
		for i := 0; i < len(dirs); i++ {
			filepath.Walk(dirs[i], walker)
		}
	}()
}

func getWalker(paths chan string) {
	return func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			paths <- string
		}
		//note: ignore errors?? ok...
		return nil
	}
}


//walker := func(path string, info os.FileInfo, err error) error {
//		out <- FileInfo{path, info}
//		return err
//	}
//	go func() {
//		e := filepath.Walk(root, walker)
//		close(out)
//		if e != nil {
//			errCh <- e
//		}
//		close(errCh)
//	}()



//Something to generate hash, filename, or other equvivalence data
//func Process(paths <-chan string) <-chan bool {
//	hash := sha256.New()
//	
//	fileHashCh := make(chan fileHash)
//	out := gather(fileHashCh)
//	go func() {
//		for path := range paths {
//			finput, err := os.Open(path)
//			if err != nil {
//				fmt.Printf("Error opening file for reading: %v\n", err)
//				continue //with the next file
//			}
//			io.Copy(hash, finput)
//			hashValue := hash.Sum(nil)
//			fmt.Printf("Computed hash for %v: %v\n", path, hashValue)
//			fileHashCh <- fileHash{path, hashValue}
//		}
//	}()
//
//	return out
//}

//Something to generate groups of files that are equvivalent in some way

//func gather(fileHash <-chan fileHash) <-chan bool {
//	out := make(chan bool)
//	go func() {
//		fileHashes := make(map[string][]byte)
//		for hashv := range fileHash {
//			fileHashes[hashv.path] = hashv.hash
//		}
//		fmt.Printf("Bah!")
//		close(out)
//	}()
//	return out
//}
