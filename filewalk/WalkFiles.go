package filewalk

import (
	"fmt"
	"os"
	"path/filepath"
)

type FileInfo struct {
	Path string
	os.FileInfo
}

func WalkFiles(root string) (<-chan FileInfo, <-chan error) {
	out := make(chan FileInfo)
	errCh := make(chan error)
	walker := func(path string, info os.FileInfo, err error) error {
		out <- FileInfo{path, info}
		return err
	}
	go func() {
		e := filepath.Walk(root, walker)
		close(out)
		if e != nil {
			errCh <- e
		}
		close(errCh)
	}()
	return out, errCh
}

func FileTypeFilter(files <-chan FileInfo, filetypes map[string]bool, accept bool) <-chan FileInfo {
	out := make(chan FileInfo)
	go func() {
		defer close(out)
		for file := range files {
			if file.IsDir() {
				out <- file
				continue
			}
			ext := filepath.Ext(file.Path)
			_, exists := filetypes[ext]
			if exists == accept {
				out <- file
			}
		}
	}()
	return out
}

func FileDirectoryFilter(files <-chan FileInfo, accept bool) <-chan FileInfo {
	out := make(chan FileInfo)
	go func() {
		defer close(out)
		for file := range files {
			if file.IsDir() {
				if accept {
					out <- file
				}
			} else {
				out <- file
			}
		}
	}()
	return out
}

func FilePrintFilter(files <-chan FileInfo) <-chan FileInfo {
	out := make(chan FileInfo)
	go func() {
		defer close(out)
		for file := range files {
			fmt.Println(file.Path)
			out <- file
		}
	}()
	return out
}

func FileSinkFilter(files <-chan FileInfo, callback func(FileInfo)) <-chan bool {
	out := make(chan bool)
	go func() {
		defer close(out)
		for file := range files {
			callback(file)
		}
	}()
	return out
}

func FileSink(files <-chan FileInfo) <-chan bool {
	return FileSinkFilter(files, func(file FileInfo) {})
}
